class WebSocketClient {
  constructor(stores) {
    this.client = null;
    this.stores = stores;
  }

  connect() {
    this.client = new WebSocket("wss://everchat-server.herokuapp.com");
    // this.client = new WebSocket("ws://localhost:8080");
    this.bindListeners();
  }

  sendMessage(message) {
    this.client.readyState !== this.client.CONNECTING &&
      this.client.send(JSON.stringify(message));
  }

  bindListeners = () => {
    this.client.onmessage = message => {
      // console.log("message was recieved", message);
      const msg = JSON.parse(message.data);
      const data = msg;
      switch (data.type) {
        case "ErrorResponse":
          this.stores.earlyAccessStore.setUserIdErrorMessage(data.message);
          break;

        case "ErrorResponseEmail":
          this.stores.earlyAccessStore.setEmailErrorMessage(data.message);
          break;

        case "ReservationSuccessResponse":
          this.stores.earlyAccessStore.setReserverdUser(data.data);
          break;

        default:
          break;
      }
    };
  };
}

export let webSocketClient;

export function createInstance(stores) {
  webSocketClient = new WebSocketClient(stores);
  webSocketClient.connect();
}
