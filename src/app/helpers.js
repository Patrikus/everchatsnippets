import firebase from "../Firebase";
import { message } from "antd";

export function createNewUser(username, password) {
  // firebase.auth().signOut();
  firebase
    .auth()
    .createUserWithEmailAndPassword(username, password)
    .then(e => {
      e.user.updateProfile({
        displayName: e.user.email.split("@")[0]
      });
      callLogin(username, password);
    })
    .catch(error => {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log(errorMessage, errorCode);
      // ...
    });
}

function callLogin(email, password) {
  firebase
    .auth()
    .signOut()
    .then(firebase.auth().signInWithEmailAndPassword(email, password));
}

export function displayWarningMessage(text) {
  message.warning(text);
}

export function randomString(len, charSet) {
  charSet =
    charSet || "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var randomString = "";
  for (var i = 0; i < len; i++) {
    var randomPoz = Math.floor(Math.random() * charSet.length);
    randomString += charSet.substring(randomPoz, randomPoz + 1);
  }
  return randomString;
}

export function validateUserId(userId) {
  const userName = userId.toString();
  if (!/^[a-zA-Z0-9 ._-]+$/.test(userId)) {
    return false;
  }
  if (
    userName.includes(" ") ||
    userName.includes("/") ||
    userName.includes("-") ||
    userName.includes(",") ||
    userName.includes("|") ||
    userName.includes("[") ||
    userName.includes("]") ||
    userName.includes("$") ||
    userName.includes("#")
  ) {
    return false;
  } else {
    return true;
  }
}

export function validateMail(email) {
  // if (email.includes("+")) {
  //   return false;
  // }
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
