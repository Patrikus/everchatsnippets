import React, { Component } from "react";
import { Provider } from "mobx-react";
import "../../App.css";
import { Route, Switch } from "react-router-dom";
import Mainheader from "./Mainheader";
// import Footer from "./Footer";
import Dashboard from "../../features/Dashboard/Components/Dashboard";
import SignInForm from "../../features/SignIn/Components/SignInForm";
import "antd/dist/antd.css";
// import { CSSTransition, TransitionGroup } from "react-transition-group";
import { withRouter } from "react-router";
import "./pageTransitions/slideTransition.css";
import Another from "./pageTransitions/Another";
import LogInForm from "../../features/SignIn/Components/LogInForm";
import ChatProfileContainer from "./ChatProfileContainer";
import EditProfile from "../../features/User/Components/EditProfile";
import firebase from "../../Firebase";
import EarlyAccessReserve from "../../features/EarlyAccess/EarlyAccessReserve";
import EarlyAccessLegal from "../../features/EarlyAccess/EarlyAccessLegal";
import EarlyAccessFeatures from "../../features/EarlyAccess/EarlyAccessFeatures";
import Custom404Component from "./Custom404Component";
import close from "../../assets/icons/close.svg";
import EarlyAccessUserContainer from "../../features/EarlyAccess/EarlyAccessUserContainer";
import { loadReCaptcha } from "react-recaptcha-google";
import EarlyAccessLanding from "../../features/EarlyAccess/EarlyAccessLanding";
import { webSocketClient, createInstance } from "../utils/webSocketClient";

class App extends Component {
  constructor(props) {
    super(props);
    this.development = true;
    this.state = {
      prevDepth: this.getPathDepth(this.props.location),
      className: "testing-nothing"
    };
  }
  state = {
    searchValue: "",
    cookies: false
  };

  //THIS WAS MOVED FROM MAIN HEADER
  componentDidMount = () => {
    const { userStore } = this.props.stores;

    if (localStorage.getItem("cookieSeen") === "shown") {
      this.setState({ cookies: true });
    }

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        userStore.setUser(user);
        setTimeout(() => {
          this.handleSetUserName(user);
        }, 500);
      }
    });

    setTimeout(() => {
      if (webSocketClient.client.readyState === 3) {
        createInstance(this.props.stores);
      }
    }, 3000);

    loadReCaptcha();
  };

  handleSetUserName(user) {
    const { userStore } = this.props.stores;
    const db = firebase.database();

    db.ref("users")
      .orderByChild("email")
      .equalTo(user.email)
      .on("value", snapshot => {
        if (snapshot.val()[user.displayName]) {
          userStore.setUserName(snapshot.val()[user.displayName].fullname);
          userStore.setUserAlias(snapshot.val()[user.displayName].alias);
        }
      });
  }

  //END OF MOVED SECTION

  componentWillReceiveProps() {
    this.setState({ prevDepth: this.getPathDepth(this.props.location) });
  }

  handleSetState = searchValue => {
    this.setState({ searchValue });
  };

  getPathDepth(location) {
    let pathArr = location.pathname.split("/");
    pathArr = pathArr.filter(n => n !== "");
    return pathArr.length;
  }

  hideCookies = () => {
    localStorage.setItem("cookieSeen", "shown");
    this.setState({ cookies: true });
  };

  render() {
    const {
      userStore,
      searchStore,
      channelStore,
      earlyAccessStore
    } = this.props.stores;

    const { location } = this.props;

    return (
      <Provider
        userStore={userStore}
        searchStore={searchStore}
        channelStore={channelStore}
        earlyAccessStore={earlyAccessStore}
      >
        {!this.development && (
          <div
            className={`route-selection ${
              this.getPathDepth(location) - this.state.prevDepth > 0
                ? "slideleft"
                : "slideright"
            }`}
          >
            {location.pathname === "/" && (
              <Mainheader handleSetState={this.handleSetState} />
            )}

            <Switch location={location}>
              <Route
                exact
                path="/"
                component={() => (
                  <Dashboard searchValue={this.state.searchValue} />
                )}
              />
              <Route path="/about" component={Another} />
              <Route exact path="/signup" component={SignInForm} />
              <Route exact path="/login" component={LogInForm} />
              {!userStore.user && (
                <Route
                  exact
                  path="/edit-profile"
                  component={() => <EditProfile withoutHistory={true} />}
                />
              )}

              {userStore.user && (
                <Route
                  exact
                  path="/edit-profile"
                  component={() => <EditProfile withoutHistory={false} />}
                />
              )}
              <Route exact path={"/:room"} component={ChatProfileContainer} />
            </Switch>
          </div>
        )}

        {this.development && (
          <Switch location={location}>
            <Route exact path="/" component={EarlyAccessLanding} />
            <Route exact path="/legal" component={EarlyAccessLegal} />
            <Route exact path="/features" component={EarlyAccessFeatures} />
            <Route exact path="/:userID" component={EarlyAccessUserContainer} />
            <Route
              exact
              path="/referral/:userID"
              component={EarlyAccessLanding}
            />
            <Route component={Custom404Component} />
          </Switch>
        )}
        <div
          className={
            this.state.cookies ? "cookie-banner cookie-hide" : "cookie-banner"
          }
        >
          <p className="cookies-text">
            We use cookies to ensure you get the best experience.{" "}
            <a href="/legal?cookies" className="cookies-anchor">
              Learn more
            </a>
          </p>
          <button onClick={this.hideCookies} className="close">
            <img src={close} alt="close button" />
          </button>
        </div>
      </Provider>
    );
  }
}

export default withRouter(App);
