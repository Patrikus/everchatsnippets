import React, { Component } from "react";
import firebase from "../../Firebase";
import Custom404Component from "../../app/components/Custom404Component";
import EarlyAccessUser from "./EarlyAccessUser";

class EarlyAccessUserContainer extends Component {
  state = {
    userExist: {}
  };
  componentDidMount() {
    const db = firebase.database();
    const userID = this.props.location.pathname.substr(1);

    db.ref("reservedUsers")
      .orderByChild("userID")
      .equalTo(userID)
      .once("value", snap => {
        if (snap.val()) {
          this.setState({ userExist: snap.val()[Object.keys(snap.val())[0]] });
        } else {
          this.setState({ userExist: undefined });
        }
      });

    db.ref("reservedUsers").once("value", snap => {
      console.log(snap.numChildren());
    });
  }
  render() {
    return (
      <div>
        {!this.state.userExist && <Custom404Component {...this.props} />}{" "}
        {this.state.userExist && (
          <EarlyAccessUser {...this.props} user={this.state.userExist} />
        )}{" "}
      </div>
    );
  }
}

export default EarlyAccessUserContainer;
