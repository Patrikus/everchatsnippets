import React, { Component } from "react";
import EarlyAccessHeader from "./EarlyAccessHeader";
import "./Styles/reserve.css";
import { Form, Input, Button } from "antd";
import { webSocketClient } from "../../app/utils/webSocketClient";
import { inject, observer } from "mobx-react";
import { validateMail, validateUserId } from "../../app/helpers";
import { ReCaptcha } from "react-recaptcha-google";

@Form.create()
@inject("earlyAccessStore")
@observer
class EarlyAccessReserve extends Component {
  constructor(props, context) {
    super(props, context);
    this.onLoadRecaptcha = this.onLoadRecaptcha.bind(this);
    this.verifyCallback = this.verifyCallback.bind(this);
  }

  state = {
    referral: undefined,
    copied: false
  };

  componentDidMount = async () => {
    if (this.props.match.params.userID) {
      this.setState({ referral: this.props.match.params.userID });
    }

    if (this.captchaDemo) {
      this.captchaDemo.reset();
      this.captchaDemo.execute();
    }
  };

  handleSubmit = () => {
    this.props.form.validateFields((errors, values) => {
      let error = false;
      if (!values.userID) {
        this.props.earlyAccessStore.setUserIdErrorMessage(
          "User ID is required"
        );
        error = true;
      } else {
        values.userID = values.userID.toLowerCase();
      }

      if (!validateUserId(values.userID)) {
        this.props.earlyAccessStore.setUserIdErrorMessage("Invalid user ID");
        error = true;
      }

      if (!values.email) {
        this.props.earlyAccessStore.setEmailErrorMessage("Email is required");
        error = true;
      } else if (!validateMail(values.email)) {
        this.props.earlyAccessStore.setEmailErrorMessage(
          "Invalid email format"
        );
        error = true;
      }

      if (!values.terms) {
        this.props.earlyAccessStore.setTermsErrorMessage("Consent is required");
        error = true;
      }

      if (error) {
        return;
      }
      if (!errors) {
        webSocketClient.sendMessage({
          type: "reserve",
          data: { ...values, referral: this.state.referral }
        });
      }
    });
  };

  handleRedirectToTerms = e => {
    e.preventDefault();
    this.props.history.push("/legal");
  };

  handleCopyToClipboard = () => {
    this.refLink.select();
    document.execCommand("copy");
    this.setState({ copied: true });
  };

  handleIdClearErrorMsg = () => {
    if (this.props.earlyAccessStore) {
    }
  };

  forceLowerCase = e => {
    e.target.value = e.target.value.toLowerCase();
  };

  onLoadRecaptcha() {
    if (this.captchaDemo) {
      this.captchaDemo.reset();
      this.captchaDemo.execute();
    }
  }
  verifyCallback(recaptchaToken) {
    // console.log(recaptchaToken);
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { earlyAccessStore } = this.props;
    return (
      <div className="routeWrapper-earlyAccess">
        <EarlyAccessHeader {...this.props} />
        <ReCaptcha
          ref={el => {
            this.captchaDemo = el;
          }}
          size="invisible"
          render="explicit"
          sitekey="6LeNeM0UAAAAAGZfUDaZkhp7ZNLOlAKyif95JggQ"
          onloadCallback={this.onLoadRecaptcha}
          verifyCallback={this.verifyCallback}
        />

        {!earlyAccessStore.email && !earlyAccessStore.userID && (
          <div>
            <Form>
              <div className="errorMessage">
                {this.props.earlyAccessStore.userIdMessage}
              </div>
              <Form.Item>
                {getFieldDecorator("userID", {})(
                  <Input
                    onFocus={this.handleIdClearErrorMsg}
                    className={
                      earlyAccessStore.userIdMessage
                        ? "earlyInput error forceLowerCase"
                        : "earlyInput forceLowerCase"
                    }
                    placeholder="Desired user ID"
                  />
                )}
              </Form.Item>

              <div className="errorMessage">
                {this.props.earlyAccessStore.emailErrorMessage}
              </div>
              <Form.Item>
                {getFieldDecorator("email", {})(
                  <Input
                    className={
                      earlyAccessStore.emailErrorMessage
                        ? "earlyInput error"
                        : "earlyInput"
                    }
                    placeholder="Your email"
                  />
                )}
              </Form.Item>

              <div className="errorMessage noBottomMargin">
                {this.props.earlyAccessStore.termsErrorMessage}
              </div>
              <Form.Item className="termsCheckboxWrapper">
                {getFieldDecorator("terms", {})(
                  <div>
                    <label className="container">
                      <input type="checkbox" />
                      <span className="checkmark" />
                    </label>
                    <span className="checkboxLabel">
                      I consent to Everchat’s{" "}
                      <a
                        href="/legal"
                        className="everchatAnchor"
                        onClick={this.handleRedirectToTerms}
                      >
                        Terms & Policies
                      </a>
                      .
                    </span>
                  </div>
                )}
              </Form.Item>
            </Form>

            <Button onClick={this.handleSubmit} className="reserveButton">
              Reserve
            </Button>
          </div>
        )}

        {earlyAccessStore.email && earlyAccessStore.userID && (
          <div className="reserveSuccessWrapper">
            <div className="reserveMessage">
              Congratulations! Your user ID{" "}
              {earlyAccessStore.userID
                ? earlyAccessStore.userID
                : "{ user_id }"}{" "}
              has been reserved for the email{" "}
              {earlyAccessStore.userID
                ? earlyAccessStore.email
                : "{ email_address }"}
            </div>

            <div className="reserverFeatureHeader">Want all the features?</div>
            <div className="reserverFeatureContent">
              Invite friends using this referral link and get 10 karma for each
              user ID reserved.
            </div>

            <div className="copyLinkWrapper">
              {this.state.copied && (
                <div className="infoMessage">Copied to clipboard!</div>
              )}
              <Input
                ref={ref => (this.refLink = ref)}
                className="copyLinkInput"
                value={`everch.at/referral/${
                  earlyAccessStore.userID
                    ? earlyAccessStore.userID
                    : "{user_id}"
                }`}
              />
              <Button
                onClick={this.handleCopyToClipboard}
                className="copylinkButton"
              >
                Copy link
              </Button>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default EarlyAccessReserve;
