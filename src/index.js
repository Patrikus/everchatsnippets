import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./app/components/App";
import * as serviceWorker from "./serviceWorker";
import UserStore from "./features/User/Store/UserStore";
import SearchStore from "./features/Dashboard/Store/searchStore";
import { BrowserRouter } from "react-router-dom";
import { createBrowserHistory } from "history";
import ChannelStore from "./features/Channel/Store/ChannelStore";
import { createInstance } from "./app/utils/webSocketClient";
import EarlyAccessStore from "./features/EarlyAccess/Store/earlyAccessStore";

const history = createBrowserHistory();

async function init() {
  const userStore = new UserStore();
  const searchStore = new SearchStore();
  const channelStore = new ChannelStore();
  const earlyAccessStore = new EarlyAccessStore();

  const stores = {
    userStore,
    searchStore,
    channelStore,
    earlyAccessStore
  };

  createInstance(stores);

  ReactDOM.render(
    <BrowserRouter history={history}>
      <App stores={stores} />
    </BrowserRouter>,
    document.getElementById("root")
  );
}

init();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
