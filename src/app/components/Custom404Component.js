import React, { Component } from "react";
import logoThumb from "../../assets/logo/thumb-logo.svg";
import "../styles/custom404page.css";
import { Button } from "antd";

class Custom404Component extends Component {
  redirectToMain = () => {
    this.props.history.push("/");
  };
  render() {
    return (
      <div>
        {" "}
        <div className="LegalHeader" onClick={this.redirectToMain}>
          <img
            className="thumbLogoHeader"
            alt="logo-thumbnail"
            src={logoThumb}
          />
        </div>
        <div className="custom404Wrapper">
          <div className="custom404Header">Page not found</div>
          <div className="custom404Content">
            <div>
              “Our lives are not our own. From womb to tomb, we are bound to
              others, past and present. And by each crime and every kindness, we
              birth our future.”
            </div>
            <div className="signature">— David Mitchell, Cloud Atlas</div>
          </div>
          <Button
            className="reserveButton home-404"
            onClick={this.redirectToMain}
          >
            Home
          </Button>
        </div>
      </div>
    );
  }
}

export default Custom404Component;
