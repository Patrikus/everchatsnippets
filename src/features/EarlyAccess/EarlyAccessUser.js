import React, { Component } from "react";
import logoThumb from "../../assets/logo/thumb-logo.svg";

class EarlyAccessUser extends Component {
  redirectToMain = () => {
    this.props.history.push("/");
  };
  render() {
    return (
      <div>
        <div className="LegalHeader">
          <img
            onClick={this.redirectToMain}
            className="thumbLogoHeader"
            alt="logo-thumbnail"
            src={logoThumb}
          />
        </div>
        <div className="profileContentWrapper">
          <div className="earlyAccess-userID">{this.props.user.userID}</div>
          <div className="earlyAccess-karma">
            {" "}
            {this.props.user.karma ? this.props.user.karma + " karma" : ""}
          </div>
        </div>
      </div>
    );
  }
}

export default EarlyAccessUser;
